#!/usr/bin/env groovy

import org.apache.poi.hssf.util.*
import org.apache.poi.hssf.usermodel.*
import java.text.SimpleDateFormat
 
@Grapes([ @Grab(group='org.apache.poi', module='poi', version='3.2-FINAL') ])
class MarcajesExcelReader {

    /* *************************************************** 
	   *          P R O G R A M A    P R I N C I P A L     * 
	   ***************************************************** */
	  public static void main(String[] args) {
          if (args.length==0)
          {
              println "Debe especificar la ruta a su archivo Excel con los marcajes"
              System.exit(-1)
          }
          File archivoExcel = new File(args[0]) 
          if (!(archivoExcel.exists()))
          {
              println "No existe el archivo que ha especificado"
              System.exit(-2)
          }
          if (!archivoExcel.getName().toLowerCase().endsWith(".xls"))
          {
              println "La ruta que ha especificado no es un archivo Excel con marcajes."
              System.exit(-3)
          }
      		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
      		def reader = new MarcajesExcelReader(archivoExcel.canonicalPath)
          def porMeses = [ : ]
          reader.eachLine {
      			    def fila = it.rowNum
        				def fecha = null
        				try { fecha = sdf.parse(cell(0)) } catch(Exception e) {}
        				if (fecha!=null && fecha[Calendar.DAY_OF_WEEK]!=Calendar.SATURDAY && fecha[Calendar.DAY_OF_WEEK]!=Calendar.SUNDAY)
        				{
                  def detalle = cell(1)
        					def retrasp = cell(3)
        					def teoricas = cell(4)
        					def pausa = cell(5)
        					def jornada = cell(6)
        					def saldo = cell(7)
                  def mesMarcaje = fecha[Calendar.MONTH]
                  // Festivos fuera
                  if (!jornada.equals('-') && !detalle.startsWith('Sin marcajes'))
                  {
            					try {
            						 def partes = saldo.split(":")
            						 if (partes.length==2)
          						 {
            							def minutos = Integer.parseInt(partes[0])
          						 	  def segundos = Integer.parseInt(partes[1])
          						 	  saldo = ((Math.abs(minutos)*60)+segundos) * (saldo.startsWith("-")?-1:1)
          						 }
            						 else 
            						 	saldo=0

            					} 
            					catch(Exception e) { 
            						saldo = 0 
            					} 
                      porMeses[mesMarcaje] = porMeses[mesMarcaje] == null ? saldo : porMeses[mesMarcaje] + saldo
                  }
        				}
      		}
          int saldoAnual = 0;
          [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
            "Septiembre", "Octubre", "Noviembre", "Diciembre" ].eachWithIndex { mes, pos ->
            int saldoMes = porMeses[pos]!=null?porMeses[pos]:0
            saldoAnual += saldoMes
            int horas = (int) Math.floor(Math.abs(saldoMes)/60)
            int minutos = Math.abs(saldoMes)-(horas*60)
            println mes+": "+(saldoMes<0?"-":"")+horas+"h. "+minutos+"' (${saldoMes} minutos)"    
          }
         int horas = (int) Math.floor(Math.abs(saldoAnual)/60)
         int minutos = Math.abs(saldoAnual)-(horas*60)
         println "--------------------------------------------"    
         println "TOTAL: "+(saldoAnual<0?"-":"")+horas+"h. "+minutos+"' (${saldoAnual} minutos)"    
    }

    /* *************************************************** 
     *     Propiedades y métodos de la clase             * 
     ***************************************************** */

    def workbook
    def labels
    def row
 
    MarcajesExcelReader(String fileName) {
        HSSFRow.metaClass.getAt = {int idx ->
            def cell = delegate.getCell(idx)
            if(! cell) {
                return null
            }
            def value
            switch(cell.cellType) {
                case HSSFCell.CELL_TYPE_NUMERIC:
                if(HSSFDateUtil.isCellDateFormatted(cell)) {
                    value = cell.dateCellValue
                } else {
                    value = cell.numericCellValue
                }
                break
                case HSSFCell.CELL_TYPE_BOOLEAN:
                value = cell.booleanCellValue
                break
                default:
                value = cell.stringCellValue
                break
            }
            return value
        }
 
        new File(fileName).withInputStream{is->
            workbook = new HSSFWorkbook(is)
        }
    }
 
    def getSheet(idx) {
        def sheet
        if(! idx) idx = 0
        if(idx instanceof Number) {
            sheet = workbook.getSheetAt(idx)
        } else if(idx ==~ /^\d+$/) {
            sheet = workbook.getSheetAt(Integer.valueOf(idx))
        } else {
            sheet = workbook.getSheet(idx)
        }
        return sheet
    }
 
    def cell(idx) {
        if(labels && (idx instanceof String)) {
            idx = labels.indexOf(idx.toLowerCase())
        }
        return row[idx]
    }
 
    def propertyMissing(String name) {
        cell(name)
    }
 
    def eachLine(Map params = [:], Closure closure) {
        def offset = params.offset ?: 0
        def max = params.max ?: 9999999
        def sheet = getSheet(params.sheet)
        def rowIterator = sheet.rowIterator()
        def linesRead = 0
 
        if(params.labels) {
            labels = rowIterator.next().collect{it.toString().toLowerCase()}
        }
        offset.times{ rowIterator.next() }
 
        closure.setDelegate(this)
 
        while(rowIterator.hasNext() && linesRead++ < max) {
            row = rowIterator.next()
            closure.call(row)
        }
    }
}